local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
  -- Theme Catppuccin
  { "catppuccin/nvim", name = "catppuccin", priority = 1000 },
  -- File explorer
  "nvim-tree/nvim-tree.lua",
  "nvim-tree/nvim-web-devicons",
  -- Nvim bottom bar
  "nvim-lualine/lualine.nvim",
  -- Code HighLight
  "nvim-treesitter/nvim-treesitter",
  -- Telescope
  {
    "nvim-telescope/telescope.nvim", tag = "0.1.2",
    dependencies = { "nvim-lua/plenary.nvim" }
  },
  -- LSP Server
  {
    "williamboman/mason.nvim",
    "williamboman/mason-lspconfig.nvim",
    "neovim/nvim-lspconfig",
  },
  -- Code completion
  "hrsh7th/nvim-cmp",
  "hrsh7th/cmp-nvim-lsp",
  "L3MON4D3/LuaSnip",
  "saadparwaiz1/cmp_luasnip",
  "rafamadriz/friendly-snippets",
  -- ToggleTerm
  {"akinsho/toggleterm.nvim", version = "*", config = true},
  -- Window Focus
  {"nvim-focus/focus.nvim", version = "*" },
  -- Tabs
  {"romgrk/barbar.nvim",
    dependencies = {
      "lewis6991/gitsigns.nvim", -- OPTIONAL: for git status
      "nvim-tree/nvim-web-devicons", -- OPTIONAL: for file icons
    },
    version = "^1.7.0", -- optional: only update when a new 1.x version is released
  },
  -- Color HighLight
  "norcalli/nvim-colorizer.lua",
  -- Nvim Startup
  {"glepnir/dashboard-nvim",
    event = 'VimEnter',
    dependencies = {
      "nvim-tree/nvim-web-devicons",
    }
  },
  -- Auto Brackets
  "m4xshen/autoclose.nvim",
  -- Indent line
  "lukas-reineke/indent-blankline.nvim",
})
