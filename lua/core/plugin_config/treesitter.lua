require 'nvim-treesitter.configs'.setup {
  -- A list of parser name, or "all"
  ensure_installed = { "astro", "bash", "css", "dockerfile", "html", "javascript", "json", "lua", "scss", "tsx", "typescript", "vue", "rust" },
  -- Install parsers synchronously (only applied to 'ensure_installed')
  sync_install = false,
  auto_install = true,
  highlight = {
    enabled = true,
  },
}
