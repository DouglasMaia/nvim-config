vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

vim.opt.backspace = '2'
vim.opt.showcmd = true
vim.opt.laststatus = 2
vim.opt.autowrite = true
vim.opt.cursorline = true
vim.opt.autoread = true

-- Use spaces for tabs and whatnot
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.shiftround = true
vim.opt.expandtab = true

-- Clear search highlight
vim.keymap.set('n', '<leader>h', ':nohlsearch<CR>')

-- Save file
vim.keymap.set('n', '<leader>w', ':w<CR>')

-- Quit neovim
vim.keymap.set('n', '<leader>q', ':q<CR>')
